
# minild69

> mini ld 69 - colonization

[![npm version](https://badge.fury.io/js/minild69.svg)](https://badge.fury.io/js/minild69)
[![Dependency Status](https://david-dm.org/mattstyles/minild69.svg)](https://david-dm.org/mattstyles/minild69)
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)

## Install

You’ll need [node and npm](https://nodejs.org/en/) installed to install and run from source. Clone the repository, install the dependencies with npm and use the start script to get going in development mode.

```sh
$ git clone git@gitlab.com:mattstyles/minild69.git
$ npm i
$ npm start
```

## Contributing

Pull requests are always welcome, the project uses the [standard](http://standardjs.com) code style.

For bugs and feature requests, [please create an issue](https://gitlab.com/mattstyles/minild69/issues).

## License

MIT
