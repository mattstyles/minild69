
import React from 'react'
import ReactDOM from 'react-dom'
import App from 'components/app'

function main () {
  let el = document.createElement('div')
  el.classList.add('js-main', 'Main')
  document.body.appendChild(el)
  return el
}

ReactDOM.render(<App />, main())
