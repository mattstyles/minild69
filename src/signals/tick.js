
import most from 'most'
import EventEmitter from 'eventemitter3'
import {fill} from 'utils/utils'

let emitter = new EventEmitter()
let signal = most
  .fromEvent('data', emitter)
  .throttle(50)

export default signal

export function dispatch (payload) {
  emitter.emit('data', payload)
}

export function toArray (granularity = 20) {
  return signal
    .scan((w, x) => {
      return w.concat(x).slice(-granularity)
    }, fill(new Array(20), 0))
}

export function smooth (granularity = 20) {
  return toArray(granularity)
    .filter(arr => arr.length > 2)
    .map(arr => arr.reduce((total, y) => total + y))
    .map(fps => fps / granularity)
}
