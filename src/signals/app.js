
import most from 'most'
import EventEmitter from 'eventemitter3'

let emitter = new EventEmitter()
let signal = most
  .fromEvent('data', emitter)

export default signal
export function dispatch (payload) {
  emitter.emit('data', payload)
}
