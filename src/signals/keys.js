
import most from 'most'
import EventEmitter from 'eventemitter3'
import Quay from 'quay'

let quay = new Quay()
let emitter = new EventEmitter()
let signal = most
  .fromEvent('data', emitter)

export default signal

quay.on('*', event => {
  emitter.emit('data', {
    keys: quay.pressed,
    dt: event.delta
  })
})
