
import React from 'react'
import Game from 'components/game/component'
import FPS from 'components/fps/fps'

const App = props => {
  return (
    <div className='js-app'>
      <span>MiniLD</span>
      <Game />
      <FPS />
    </div>
  )
}

export default App
