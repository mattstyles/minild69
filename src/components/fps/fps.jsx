
import React, {Component} from 'react'
import {
  smooth as smoothTick,
  toArray as tickArray
} from 'signals/tick'

const MS = 1000

export default class FPS extends Component {
  constructor () {
    super()

    tickArray().observe(ticks => {
      this.setState({
        ticks: ticks
      })
    })

    smoothTick().observe(dt => {
      this.setState({
        frames: MS / dt | 0
      })
    })
  }

  state = {
    frames: 0,
    ticks: []
  }

  render () {
    let {frames} = this.state
    let ticks = this.state.ticks
      .map((tick, index) => {
        return (
          <span key={'tick' + index} className='FPS-tick' style={{
            height: (60 / (1000 / tick)) * 8
          }}></span>
        )
      })
    return (
      <div className='FPS'>
        <span className='FPS-title'>{frames}</span>
        {ticks}
      </div>
    )
  }
}
