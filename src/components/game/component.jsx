
import Pixi from 'pixi.js'
import {Component} from 'React'

import keys from 'signals/keys'
import game from './game'
import stage from './stage'

function double (x, y) {
  return [x * 2, y * 2]
}

export default class Game extends Component {
  componentDidMount () {
    game.start()

    let graphics = new Pixi.Graphics()
    graphics.beginFill(0xff4400)
    graphics.drawCircle(200, 100, 30)
    graphics.endFill()

    let texture = new Pixi.RenderTexture(game.renderer, ...double(...game.shape))
    let tilemap = new Pixi.Sprite(texture)
    texture.render(graphics, null, false)

    stage.addChild(tilemap)

    keys.observe(event => {
      if (event.keys.has('<up>')) {
        tilemap.y--
      }

      if (event.keys.has('<down>')) {
        tilemap.y++
      }

      if (event.keys.has('<left>')) {
        tilemap.x--
      }

      if (event.keys.has('<right>')) {
        tilemap.x++
      }
    })
  }

  render () {
    return null
  }
}
