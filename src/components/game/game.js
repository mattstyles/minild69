
import {dispatch as tickDispatch} from 'signals/tick'
import Renderers from 'utils/renderer'
import {debug} from 'utils/debug'
import stage from './stage'

let game = Renderers.create('js-game')
debug('game', game)

game.on('resize', () => {
  game.renderer.resize(...game)
})

game.on('tick', dt => {
  tickDispatch(dt)
  game.renderer.render(stage)
})

export default game
