
import Pixi from 'pixi.js'
import Renderers from 'utils/renderer'

let stage = new Pixi.Container()

let graphics = new Pixi.Graphics()
graphics.beginFill()
graphics.drawCircle(100, 100, 50)
graphics.endFill()
// stage.addChild(graphics)

let renderer = Renderers.get('js-main')
// let texture = new Pixi.RenderTexture(renderer, renderer.width, renderer.height)
// let tilemap = new Pixi.Sprite(texture)

// let container = new Pixi.Container()
// container.addChild(graphics)

// texture.render(graphics, null, false)
// stage.addChild(tilemap)

export default stage
