
export let debugInfo = new Map()
export function debug (key, value) {
  if (!process.env.DEBUG) {
    return
  }

  debugInfo.set(key, value)
}

if (process.env.DEBUG) {
  window.g = debugInfo
}
