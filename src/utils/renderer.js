
import Pixi from 'pixi.js'
import loop from 'canvas-loop'
import canvas from './canvas'
import {debug} from 'utils/debug'

/**
 * Manages Pixi renderers
 */

let renderers = new Map()

debug('renderers', renderers)

export function create (id = 'js-main') {
  if (renderers.has(id)) {
    throw new Error('Renderer ID already in use: ' + id)
  }

  let resolution = window.devicePixelRatio
  let view = canvas.create(id)
  let app = loop(view, {
    scale: resolution
  })
  let renderer = Pixi.autoDetectRenderer(...app.shape, {
    antialiasing: false,
    transparent: false,
    resolution,
    view,
    backgroundColor: 0x181818
  })

  app.view = view
  app.renderer = renderer

  renderers.set(id, app)
  return app
}

export function get (id = 'js-main') {
  if (!renderers.has(id)) {
    return create(id)
  }

  return renderers.get(id)
}

export default {
  create,
  get
}
