
import {debug} from 'utils/debug'

/**
 * Canvas management methods
 */

let canvases = new Map()

debug('canvases', canvases)

export function create (id = 'js-canvas', parent = document.body) {
  if (canvases.has(id)) {
    throw new Error('Canvas ID already in use: ' + id)
  }

  let canvas = document.createElement('canvas')
  canvas.classList.add(id)
  parent.appendChild(canvas)
  canvases.set(id, canvas)
  return canvas
}

export function get (id = 'js-canvas') {
  if (!canvases.has(id)) {
    return create(id)
  }

  return canvases.get(id)
}

export default {
  create,
  get
}
