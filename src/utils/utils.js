
export function fill (arr, val) {
  let i = arr.length
  while (--i >= 0) {
    arr[i] = val
  }
  return arr
}
